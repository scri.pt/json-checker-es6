
import fs from 'fs';
import util from 'util';
import path from 'path';
import chai from 'chai';
import glob from 'glob';
import json_checker from '../index';
import promises from 'chai-as-promised';

/**
 * @name globAsync:
 *   Promise-based wrapper for the `glob` module.
 */
const globAsync = util.promisify(glob);

/**
 * @name readFileAsync:
 *   Promise-based wrapper for the built-in `fs.readFile`.
 */
const readFileAsync = util.promisify(fs.readFile);

/**
 * @name get_fixtures:
 *   Asynchronously read and return an array of JSON fixtures.
 */
const get_fixtures = (_type) => {

  let pattern = path.join(
    path.dirname(__filename), 'fixtures', `${_type}*.json`
  );

  return globAsync(pattern).then((_paths) => {
    return Promise.all(_paths.map((_path) => {
      return readFileAsync(_path).then((_b) => _b.toString());
    }));
  });
};

/**
 * @name check_fixtures:
 *   Parse each JSON fixture in the `_fixtures` array, ensuring
 *   that the string is recognized as valid iff `_is_valid` is true.
 *   If this group of test fixtures has no truncated-but-otherwise-valid
 *   JSON in it, set `_no_trailing` to true to perform a more strict
 *   set of assertions (specifically, checking the return value of `put`
 *   even when `_is_valid` is false).
 */
const check_fixtures = (_fixtures, _is_valid, _no_trailing) => {

  let a = new json_checker(20);

  /* For every JSON document of this type... */
  for (let i = 0, l1 = _fixtures.length; i < l1; ++i) {

    let rv = a.put(_fixtures[i]);

    if (_is_valid || _no_trailing) {
      rv.should.equal(_is_valid);
    }

    a.done().should.equal(_is_valid);
    a.reset().should.equal(a);
  }

  return true;
};

/**
 * @name json-checker:
 *   Primarily fixture-based unit tests for the JSON DPDA.
 */
describe('json-checker', () => {

  chai.use(promises);
  let should = chai.should();

  /**
   * @name retrieves the state:
   */
  it('retrieves the state', () => {

    let a = new json_checker();
    a.state.should.be.a('number');
  });

  /**
   * @name retrieves the stach:
   */
  it('retrieves the stack', () => {

    let a = new json_checker();
    a.stack.should.be.an('array');
  });

  /**
   * @name rejects the empty string:
   */
  it('rejects the empty string', () => {

    let a = new json_checker();
    a.putc("".charCodeAt(0)).should.equal(false);
    a.done().should.equal(false);
  });

  /**
   * @name accepts valid JSON fixtures:
   */
  it('accepts valid JSON fixtures', () => {

    return get_fixtures('pass').then((_f) => {
      return check_fixtures(_f, true);
    });
  });

  /**
   * @name rejects invalid JSON fixtures:
   */
  it('rejects invalid JSON fixtures', () => {

    return get_fixtures('fail').then((_f) => {
      return check_fixtures(_f, false);
    });
  });
});

/* vim: set expandtab sw=2 ts=8 sts=2: */
