
# json-checker-es6

The json-checker-es6 package is a Deterministic Pushdown Automaton (DPDA) that
recognizes JSON, written in ES6 Javascript. This is a port from the original
2005 json.org C-based implementation of the DPDA.

This version has been modified to allow for the enumeration of all possible
non-failing state transitions at any given point during parsing.

