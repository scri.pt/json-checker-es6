/*
  json-checker.js - 2018-05-05
  Ported from the JSON_Checker - 2007-08-24
    
  Copyright (c) 2005 JSON.org
  Copyright (c) 2018 David Brown

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  Be nice to each other.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

const __ = -1; /* the universal error code */

/*
 * @name classes:
 *   Characters are mapped into these 31 character classes. This
 *   allows for a significant reduction in the size of the state
 *   transition table. Don't bother simulating an enum; flat is fast.
 */
const C_SPACE = 0;  /* space */
const C_WHITE = 1;  /* other whitespace */
const C_LCURB = 2;  /* {  */
const C_RCURB = 3;  /* } */
const C_LSQRB = 4;  /* [ */
const C_RSQRB = 5;  /* ] */
const C_COLON = 6;  /* : */
const C_COMMA = 7;  /* , */
const C_QUOTE = 8;  /* " */
const C_BACKS = 9;  /* \ */
const C_SLASH = 10; /* / */
const C_PLUS  = 11; /* + */
const C_MINUS = 12; /* - */
const C_POINT = 13; /* . */
const C_ZERO  = 14; /* 0 */
const C_DIGIT = 15; /* 123456789 */
const C_LOW_A = 16; /* a */
const C_LOW_B = 17; /* b */
const C_LOW_C = 18; /* c */
const C_LOW_D = 19; /* d */
const C_LOW_E = 20; /* e */
const C_LOW_F = 21; /* f */
const C_LOW_L = 22; /* l */
const C_LOW_N = 23; /* n */
const C_LOW_R = 24; /* r */
const C_LOW_S = 25; /* s */
const C_LOW_T = 26; /* t */
const C_LOW_U = 27; /* u */
const C_ABCDF = 28; /* ABCDF */
const C_E     = 29; /* E */
const C_ETC   = 30; /* everything else */
const NR_CLASSES = 31;

/*
 * @name ascii_class:
 *   This array maps the 128 ASCII characters into character
 *   classes. The remaining Unicode characters should be mapped
 *   to C_ETC. Non-whitespace control characters are errors.
 */
const ascii_class = [
  __,      __,      __,      __,      __,      __,      __,      __,
  __,      C_WHITE, C_WHITE, __,      __,      C_WHITE, __,      __,
  __,      __,      __,      __,      __,      __,      __,      __,
  __,      __,      __,      __,      __,      __,      __,      __,

  C_SPACE, C_ETC,   C_QUOTE, C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,
  C_ETC,   C_ETC,   C_ETC,   C_PLUS,  C_COMMA, C_MINUS, C_POINT, C_SLASH,
  C_ZERO,  C_DIGIT, C_DIGIT, C_DIGIT, C_DIGIT, C_DIGIT, C_DIGIT, C_DIGIT,
  C_DIGIT, C_DIGIT, C_COLON, C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,

  C_ETC,   C_ABCDF, C_ABCDF, C_ABCDF, C_ABCDF, C_E,     C_ABCDF, C_ETC,
  C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,
  C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_ETC,
  C_ETC,   C_ETC,   C_ETC,   C_LSQRB, C_BACKS, C_RSQRB, C_ETC,   C_ETC,

  C_ETC,   C_LOW_A, C_LOW_B, C_LOW_C, C_LOW_D, C_LOW_E, C_LOW_F, C_ETC,
  C_ETC,   C_ETC,   C_ETC,   C_ETC,   C_LOW_L, C_ETC,   C_LOW_N, C_ETC,
  C_ETC,   C_ETC,   C_LOW_R, C_LOW_S, C_LOW_T, C_LOW_U, C_ETC,   C_ETC,
  C_ETC,   C_ETC,   C_ETC,   C_LCURB, C_ETC,   C_RCURB, C_ETC,   C_ETC
];

/*
 * @name states:
 *   The state codes, to be used on the DPDA's stack.
 *   Again, don't get fancy with objects here: flat is fast.
 */
const GO = 0;  /* start    */
const OK = 1;  /* ok       */
const OB = 2;  /* object   */
const KE = 3;  /* key      */
const CO = 4;  /* colon    */
const VA = 5;  /* value    */
const AR = 6;  /* array    */
const ST = 7;  /* string   */
const ES = 8;  /* escape   */
const U1 = 9;  /* u1       */
const U2 = 10; /* u2       */
const U3 = 11; /* u3       */
const U4 = 12; /* u4       */
const MI = 13; /* minus    */
const ZE = 14; /* zero     */
const IN = 15; /* integer  */
const FR = 16; /* fraction */
const E1 = 17; /* e        */
const E2 = 18; /* ex       */
const E3 = 19; /* exp      */
const T1 = 20; /* tr       */
const T2 = 21; /* tru      */
const T3 = 22; /* true     */
const F1 = 23; /* fa       */
const F2 = 24; /* fal      */
const F3 = 25; /* fals     */
const F4 = 26; /* false    */
const N1 = 27; /* nu       */
const N2 = 28; /* nul      */
const N3 = 29; /* null     */
const NR_STATES = 30;

/*
 * @state_transition_table:
 *   This is the finite state machine for the JSON pushdown automata
 *   recognizer. The state transition table takes the current state and
 *   the current symbol, and returns either a new state or an action. An
 *   action is represented as a negative number. A JSON text is accepted
 *   if at the end of the text the state is OK and if the mode is MODE_DONE.
 */
const state_transition_table = [
/*               white                                      1-9                                   ABCDF  etc
             space |  {  }  [  ]  :  ,  "  \  /  +  -  .  0  |  a  b  c  d  e  f  l  n  r  s  t  u  |  E  |*/
/*start  GO*/ [GO,GO,-6,__,-5,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*ok     OK*/ [OK,OK,__,-8,__,-7,__,-3,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*object OB*/ [OB,OB,__,-9,__,__,__,__,ST,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*key    KE*/ [KE,KE,__,__,__,__,__,__,ST,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*colon  CO*/ [CO,CO,__,__,__,__,-2,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*value  VA*/ [VA,VA,-6,__,-5,__,__,__,ST,__,__,__,MI,__,ZE,IN,__,__,__,__,__,F1,__,N1,__,__,T1,__,__,__,__],
/*array  AR*/ [AR,AR,-6,__,-5,-7,__,__,ST,__,__,__,MI,__,ZE,IN,__,__,__,__,__,F1,__,N1,__,__,T1,__,__,__,__],
/*string ST*/ [ST,__,ST,ST,ST,ST,ST,ST,-4,ES,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST,ST],
/*escape ES*/ [__,__,__,__,__,__,__,__,ST,ST,ST,__,__,__,__,__,__,ST,__,__,__,ST,__,ST,ST,__,ST,U1,__,__,__],
/*u1     U1*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,U2,U2,U2,U2,U2,U2,U2,U2,__,__,__,__,__,__,U2,U2,__],
/*u2     U2*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,U3,U3,U3,U3,U3,U3,U3,U3,__,__,__,__,__,__,U3,U3,__],
/*u3     U3*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,U4,U4,U4,U4,U4,U4,U4,U4,__,__,__,__,__,__,U4,U4,__],
/*u4     U4*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,ST,ST,ST,ST,ST,ST,ST,ST,__,__,__,__,__,__,ST,ST,__],
/*minus  MI*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,ZE,IN,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*zero   ZE*/ [OK,OK,__,-8,__,-7,__,-3,__,__,__,__,__,FR,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*int    IN*/ [OK,OK,__,-8,__,-7,__,-3,__,__,__,__,__,FR,IN,IN,__,__,__,__,E1,__,__,__,__,__,__,__,__,E1,__],
/*frac   FR*/ [OK,OK,__,-8,__,-7,__,-3,__,__,__,__,__,__,FR,FR,__,__,__,__,E1,__,__,__,__,__,__,__,__,E1,__],
/*e      E1*/ [__,__,__,__,__,__,__,__,__,__,__,E2,E2,__,E3,E3,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*ex     E2*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,E3,E3,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*exp    E3*/ [OK,OK,__,-8,__,-7,__,-3,__,__,__,__,__,__,E3,E3,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*tr     T1*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,T2,__,__,__,__,__,__],
/*tru    T2*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,T3,__,__,__],
/*true   T3*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,OK,__,__,__,__,__,__,__,__,__,__],
/*fa     F1*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,F2,__,__,__,__,__,__,__,__,__,__,__,__,__,__],
/*fal    F2*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,F3,__,__,__,__,__,__,__,__],
/*fals   F3*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,F4,__,__,__,__,__],
/*false  F4*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,OK,__,__,__,__,__,__,__,__,__,__],
/*nu     N1*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,N2,__,__,__],
/*nul    N2*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,N3,__,__,__,__,__,__,__,__],
/*null   N3*/ [__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,__,OK,__,__,__,__,__,__,__,__]
];

/*
 * @name modes:
 *   These modes can be pushed on the stack.
 */
const MODE_ARRAY = 0;
const MODE_DONE = 1;
const MODE_KEY = 2;
const MODE_OBJECT = 3;

/**
 * @name Automaton:
 */
class Automaton {

  /*
   * @name constructor:
   *  Creare a new JSON-recognizing automaton. If the `_depth` parameter
   *  is supplied, it will enforce an upper limit on the number of PDA
   *  stack frames (and therefore memory usage); this roughly corresponds
   *  to JSON nesting levels. To parse, call `char` for each character
   *  in the JSON text, and then call `done` to obtain the final result.
   */
  constructor (_depth) {

    this._depth = (_depth || 0);
    return this.reset();
  }

  /**
   * @name reset:
   */
  reset () {

    this._state = GO;
    this._is_rejected = false;
    this._stack = [ MODE_DONE ];

    return this;
  }

  /**
   * @name state:
   */
  get state () {

    return this._state;
  }

  /**
   * @name stack:
   */
  get stack () {

    return this._stack;
  }

  /**
   * @name put:
   *  Call `putc` for every character code in `_s`, in order.
   */
  put (_s) {

    for (let i = 0, len = _s.length; i < len; ++i) {
      if (!this.putc(_s.charCodeAt(i))) {
        return false;
      }
    }

    return true;
  }

  /**
   * @name putc:
   *  After instantiating this class, call this function for each
   *  integer character codepoint in your JSON text. It can accept
   *  UTF-8, UTF-16, or UTF-32. It returns true if things are looking ok
   *  so far. If it rejects the text, this method will return false.
   */
  putc (_next_charcode) {

    let next_class;
  
    /* Determine the character's class. */
    if (isNaN(_next_charcode)) {
      return this.reject();
    }
    if (_next_charcode >= 128) {
      next_class = C_ETC;
    } else {
      next_class = ascii_class[_next_charcode];
      if (next_class <= __) {
        return this.reject();
      }
    }

    /* Get the next state from the state transition table.  */
    let next_state = state_transition_table[this._state][next_class];

    if (next_state >= 0) {

      /* Fast path: no stack */
      this._state = next_state;
      return true;
    }

    /* Or, perform one of the actions... */
    switch (next_state) {

      /* empty } */
      case -9:
        if (!this.pop(MODE_KEY)) {
          return this.reject();
        }
        this._state = OK;
        break;

      /* } */
      case -8:
        if (!this.pop(MODE_OBJECT)) {
          return this.reject();
        }
        this._state = OK;
        break;

      /* ] */
      case -7:
        if (!this.pop(MODE_ARRAY)) {
          return this.reject();
        }
        this._state = OK;
        break;

      /* { */
      case -6:
        if (!this.push(MODE_KEY)) {
          return this.reject();
        }
        this._state = OB;
        break;

      /* [ */
      case -5:
        if (!this.push(MODE_ARRAY)) {
          return this.reject();
        }
        this._state = AR;
        break;

      /* " */
      case -4:
        switch (this.peek()) {
          case MODE_KEY:
            this._state = CO;
            break;
          case MODE_ARRAY:
          case MODE_OBJECT:
            this._state = OK;
            break;
          default:
            return this.reject();
        }
        break;

      /* , */
      case -3:
        switch (this.peek()) {
          case MODE_OBJECT:
            /* A comma causes a flip from object mode to key mode */
            if (!this.pop(MODE_OBJECT) || !this.push(MODE_KEY)) {
              return this.reject();
            }
            this._state = KE;
            break;
          case MODE_ARRAY:
            this._state = VA;
            break;
          default:
            return this.reject();
        }
        break;

      /* : */
      case -2:
        /* A colon causes a flip from key mode to object mode */
        if (!this.pop(MODE_KEY) || !this.push(MODE_OBJECT)) {
          return this.reject();
        }
        this._state = VA;
        break;

      /* Bad action */
      default:
        return this.reject();
    }

    return true;
  }

  /**
   * @name done:
   *   Call this function only after you have processed all characters
   *   in your JSON text using the `char` method, and only after all
   *   calls to `char` have returned true. This function returns true
   *   if the JSON text was accepted; false otherwise.
   */
  done () {

    return (
      !this._is_rejected &&
        (this._state == OK && this.pop(MODE_DONE))
    );
  }

  /** @protected: **/

  /**
   * @name reject:
   *   Free any held resources and return false.
   */
  reject () {

    this._is_rejected = true;
    return false;
  }

  /**
   * @name push:
   *   Push a mode onto the stack. Return false if there is overflow.
   */
  push (_mode) {

    if (this._depth && this._depth <= this._stack.length) {
      return false;
    }

    this._stack.push(_mode);
    return true;
  }

  /**
   * @name pop:
   *   Pop the stack, assuring that the current mode matches the
   *   expected mode `_mode`. Return false if there is underflow
   *   or if the modes mismatch.
   */
  pop (_mode) {

    let rv = this._stack.pop();
    return (rv === _mode);
  }

  /**
   * @name peek:
   *   Peek at the top of the stack without modifying anything.
   *   Returns the value at the top of the stack, or `false` if
   *   there is underflow.
   */
  peek () {

    let len = this._stack.length;
    return (len <= 0 ? false : this._stack[len - 1]);
  }
}

export {
  Automaton,
  ascii_class,
  state_transition_table
};

export default Automaton;

/* vim: set expandtab nowrap ts=8 tw=0 sts=2: */
